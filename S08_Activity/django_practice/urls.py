from django.urls import path
from . import views

urlpatterns = [
    path('hello/', views.hello_message),
    path('', views.index, name='index'),  # This line is added to map the empty path to your index view
]
