from django.http import HttpResponse
from django.shortcuts import render
from .models import GroceryItem

# Create your views here.
def hello_message(request):
    return HttpResponse("Hello from the views.py file!")

def index(request):
    items = GroceryItem.objects.all()
    return render(request, 'django_practice/index.html', {'items': items})
    